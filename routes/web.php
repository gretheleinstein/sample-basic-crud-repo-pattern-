<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('all-task', 'TaskController@get_all_task');
Route::post('create_task', 'TaskController@create_task');
Route::get('get_edit', 'TaskController@get_edit_task');
Route::post('edit_task', 'TaskController@edit_task');
Route::get('delete_task', 'TaskController@delete_task');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
