<?php

namespace App\Http\Controllers;

use App\Repositories\TaskRepository;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $model;

    /**
     * TaskController constructor.
     * @param Task $task
     */
    public function __construct(TaskRepository $repo)
    {
        $this->model = $repo;
    }

    public function create_task(Request $request){
        $this->model->create($request->except('_token'));
        return redirect('all-task');
    }

    public function get_all_task(){
        return view('tasks')->with('data', $this->model->get_all() );
    }

    public function get_edit_task(Request $request){
        return json_encode($this->model->get_by_id($request->get('id')));
    }

    public function edit_task(Request $request){
        $this->model->update($request->get('id'),$request->except(['_token', 'id']));
        return redirect('all-task');
    }

    public function delete_task(Request $request){
        return $this->model->delete($request->get('id'));
    }
}
