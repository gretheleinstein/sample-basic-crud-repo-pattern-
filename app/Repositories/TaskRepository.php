<?php
/**
 * Created by PhpStorm.
 * User: Grethel
 * Date: 4/23/2019
 * Time: 5:40 PM
 */

namespace App\Repositories;


interface TaskRepository
{
    public function get_all();

    public function get_by_id($id);

    public function create($attr);

    public function update($id, $attr);

    public function delete($id);

}