<?php
/**
 * Created by PhpStorm.
 * User: Grethel
 * Date: 4/23/2019
 * Time: 5:41 PM
 */

namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;
//use TaskRepository;
use App\Task;

class TaskEloquent implements \App\Repositories\TaskRepository
{
    /**
     * @var Model
     */
    private $model;

    public function __construct(Task $model)
    {
        $this->model = $model;
    }

    public function get_all()
    {
        return $this->model->all();
    }

    public function get_by_id($id)
    {
        return $this->model->where('id', '=', $id)->first();
    }

    public function create($attr)
    {
        return $this->model->create($attr);
    }

    public function update($id, $attr)
    {
        $this->get_by_id($id)->update($attr);
    }

    public function delete($id)
    {
        $this->model->destroy($id);
        return 'true';
    }
}