@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <button class="btn btn-default pull-right" data-toggle="modal" data-target="#createModal">
                            Add new
                        </button><br>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Task Name</th>
                                <th scope="col">Task Notes</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $ctr = 1 ?>
                            @foreach($data as $d)
                                <tr>
                                    <th scope="row">{{ $ctr++ }}</th>
                                    <td>{{ $d->task_name }}</td>
                                    <td>{{ $d->task_note }}</td>
                                    <td><button class="btn btn-default" onclick="edit({{ $d->id }})" data-toggle="modal" data-target="#myModal">Edit</button> &nbsp;
                                        <button class="btn btn-default" onclick="delete_record({{ $d->id }})">Delete</button></td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Create Modal -->
    <div id="createModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form method="POST" action="{{ url('create_task') }}">@csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <label>Task Name</label>
                        <input name="task_name" id="task_name" type="text" class="form-control"><br>
                        <label>Task Note</label>
                        <input name="task_note" id="task_note" type="text" class="form-control">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form method="POST" action="{{ url('edit_task') }}" id="edit_form">@csrf
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label>Task Name</label>
                    <input name="task_name" id="task_name" type="text" class="form-control"><br>
                    <label>Task Note</label>
                    <input name="task_note" id="task_note" type="text" class="form-control">
                    <input name="id" id="task_id" type="hidden">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>

        </div>
    </div>

    <script>
        function edit(id) {
            $.ajax({
                url: "{{ url('get_edit') }}",
                method: "GET",
                data: { id: id},
                success: function (data) {
                    $result = JSON.parse(data);
                    $("#edit_form #task_name").val($result['task_name']);
                    $("#edit_form #task_note").val($result['task_note']);
                    $("#edit_form #task_id").val($result['id']);
                },
                error: function (e) {
                    console.log(e);
                }

            });
        }
        function delete_record(id){
            $.ajax({
                url: "{{ url('delete_task') }}",
                method: "GET",
                data: { id: id},
                success: function (data) {
                    var result = data;
                    if(result){
                        document.location.reload();
                    }else{
                        alert(result);
                    }
                },
                error: function (e) {
                    console.log(e);
                }

            });
        }

        function editx(id) {
            $.ajax({
                url: "{{ url('edit_task') }}",
                method: "GET",
                data: {id: '58', arr_data: $('#edit_form').serialize()},
                success: function (data) {
                    var result = data;
                    if (result) {
//                    document.location.reload();
                    } else {
                        alert(result);
                    }
                },
                error: function (e) {
                    console.log(e);
                }

            });
        }
    </script>
@endsection
