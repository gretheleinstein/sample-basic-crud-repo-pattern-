<?php

use Faker\Generator as Faker;
use App\Task;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
            'task_name' => $faker->word,
            'task_note' => $faker->sentence(3, true),
    ];
});
